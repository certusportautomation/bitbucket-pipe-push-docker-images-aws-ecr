#!/usr/bin/env bash

# Define colors used in building logs	 
HIGHLIGHT_YELLOW='\033[0;33m'
HIGHLIGHT_GREEN='\033[0;32m'

# Set high expectations so that Pipelines fails on an error or exit 1
set -e
set -o pipefail

printf "${HIGHLIGHT_YELLOW}Logging into AWS ECR... \n"
pip3 install awscli
aws configure set aws_access_key_id "${AWS_KEY}"
aws configure set aws_secret_access_key "${AWS_SECRET}"
eval $(aws ecr get-login --no-include-email --region ${AWS_REGION} | sed 's;https://;;g')

# Navigate to the directory containing docker images
cd $DOCKER_IMAGES_DIR

# Iterate through all of the docker images
for di in $(ls -d */ | sed 's#/##') # List all directories without forward slash
do 
	IMAGE_URL=$AWS_ECR_URL/$di
	aws ecr describe-repositories --region $AWS_REGION --repository-names $di || aws ecr create-repository --region $AWS_REGION --repository-name $di # Create repository if it does not exist
	
	cd $di
	printf "${HIGHLIGHT_YELLOW}Building and pushing docker image for '$di... \n"
	docker build -t $IMAGE_URL .

	for tag in $BITBUCKET_BUILD_NUMBER latest # Pushes the build number and 'latest'
	do
		docker tag $IMAGE_URL $IMAGE_URL:$tag
	done 
	
	docker push $IMAGE_URL:$tag
	
	cd ../
done

printf "${HIGHLIGHT_GREEN} Script executed succesfully! \n"
printf $? # exit code
# Bitbucket Pipelines Pipe: cepass/push-docker-images-aws-ecr

Pushing docker images to Amazon Elastic container registry.

## Variables
 
| Variable              | Description           |
| --------------------- | ----------------------------------------------------------- |
| AWS_ECR_URL   	| AWS URL to ECR (Without any repository name at then end)	|
| AWS_KEY 			| AWS IAM key |
| AWS_REGION   		| AWS Region	|
| AWS_SECRET     	| AWS IAM secret |
| DOCKER_IMAGES_DIR | Path to the folder container directories that include a dockerfile. Each directory name is set to the name of the image. |

## YAML Definition (Using repository variables)

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
- pipe: docker://cepass/push-docker-images-aws-ecr
  variables:
    DOCKER_IMAGES_DIR: $DOCKER_IMAGES_DIR
    AWS_ECR_URL: $AWS_ECR_URL
    AWS_REGION: $AWS_REGION
    AWS_KEY: $AWS_KEY
    AWS_SECRET: $AWS_SECRET
```

## Support

If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by pass@certusportautomation.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
